/* Program: LudoXtreme_PawnStranding.c
 * Deskripsi: Aplikasi permainan Ludo Extreme
 * Author: Afdal Ramdan Daman Huri & Aldy Akbarrizky
 * Tanggal dibuat: 22 November 2019
 * Tanggal selesai: -
 * Versi: alpha 1.0
 * Compiler: Dev C++ 5.11
 */
 
#include<stdio.h>
#include<windows.h>

void gotoxy(short x, short y) {
	COORD pos = {x, y};
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
}

int main()
{
	printf("Ubah pertama oleh Afdal Ramdan Daman Huri");
	return 0;
}
